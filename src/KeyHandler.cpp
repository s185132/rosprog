#include "ros/ros.h"
#include <iostream>
#include <stdio.h>

class IKeyPressListerner {
    public:
    virtual ~IKeyPressListerner(){};
    virtual void up()   = 0;
    virtual void down() = 0;
    virtual void left() = 0;
    virtual void right()= 0;
};

class KeyHandler{

    private:
    IKeyPressListerner* list;

    public:
    KeyHandler(IKeyPressListerner& listener){
        list = &listener;
    };

    void handleKey(){
        //list->up(); 
        // System command found at 
        // https://www.tutorialspoint.com/Read-a-character-from-standard-input-without-waiting-for-a-newline-in-Cplusplus

        cout << "pres [WASD] to controll, and pres [.] to terminate (tested using : Danish WIN keyboard layout)" << endl;
        char key;
        std::system("stty raw");
        while(1){
            key = getchar();
            switch (key){
            
            // UP 
            case 'w':
                //cout <<"UP\n" << endl;
                list->up();
                break;
            
            // DOWN
            case 's':
                //cout <<"down\n" << endl;
                list->down();
                break;
            
            // RIGHT
            case 'd':
                //cout <<"right\n" << endl;
                list->right();
                break;
            
            // LEFT
            case 'a':
                //cout <<"left\n" << endl;
                list->left();
                break;
            
            // END
            case '.':
                std::system("stty cooked");
                exit(0);
                break;
            
            default:
                break;
            }
        }
    };
};