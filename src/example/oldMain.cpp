#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include <sstream>
#include "sensor_msgs/LaserScan.h"


geometry_msgs::Twist createDirectMsg(double x, double y);
geometry_msgs::Twist createCircularMsg();

int main(int argc, char **argv){
  
  ros::init(argc, argv, "talker");
  ros::NodeHandle n;
  ros::Publisher publ = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);
    
    int breakTime = 2;
    sleep(breakTime);
    publ.publish(createDirectMsg(2,0));
    sleep(breakTime);
    publ.publish(createDirectMsg(0,2));
    sleep(breakTime);
    publ.publish(createDirectMsg(-2,0));
    sleep(breakTime);
    publ.publish(createDirectMsg(0,-2));
    sleep(breakTime +1 );
    publ.publish(createDirectMsg(1,0));
    sleep(breakTime);

    int a = 0;
    while(a < 10){
        publ.publish(createCircularMsg());
         sleep(breakTime);
        a++;
    }
  return 0;
}

geometry_msgs::Twist createDirectMsg(double x, double y){
    geometry_msgs::Twist msg;
    msg.linear.x = x ;
    msg.linear.y = y ;
    msg.linear.z = 0 ;

    msg.angular.x = 0 ;
    msg.angular.y = 0 ;
    msg.angular.z = 0 ;
    return msg;
}

geometry_msgs::Twist createCircularMsg(){
geometry_msgs::Twist msg;
    msg.linear.x = 1 ;
    msg.linear.y = 0 ;
    msg.linear.z = 0 ;

    msg.angular.x = 0 ;
    msg.angular.y = 0 ;
    msg.angular.z = 1 ;
    return msg;
}
