#include "ros/ros.h"
#include "IO/RClient.cpp"
#include <iostream>
#include "KeyHandler.cpp"

using namespace std;

class MainClass : public IKeyPressListerner {
    
    private:
    RClient client;
    double  MOVE_rot_factor = 1.0;
    int     MOVE_fwrd_factor = 2    ;

    public:
    MainClass(int argc, char **argv){
        client.setChannel(false);
    };
    
    void up(){
        cout << "\nUP\n" << endl;
        client.moveMessage(  MOVE_fwrd_factor ,0);
    };
    void down(){
        cout << "\nDOWN\n" << endl;
        client.moveMessage( - MOVE_fwrd_factor ,0);
    };
    void left(){
        cout << "\nLEFT\n" << endl;
        client.rotMessage(MOVE_rot_factor);
    };
    void right(){
        cout << "\nRIGHT\n" << endl;
        client.rotMessage(-MOVE_rot_factor);
    };
};

int main(int argc, char **argv){

    ros::init(argc, argv, "talker");
    MainClass mainClass = MainClass( argc, argv);
    KeyHandler k( mainClass );
    k.handleKey();

};



