#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include <sstream>
#include <string.h>

using namespace std;
class RClient{

    private:
    ros::NodeHandle n;
    int buffer_size = 5;

    // Topics 
    string TOPIC_MOVE = "/cmd_vel";
    string TOPIC_TURTLE_MOVE ="/turtle1/cmd_vel";

    // Publishers 
    ros::Publisher publisher_move;

    public:
    ~RClient(){}
    void setChannel(bool isTurtleSim){
         if(isTurtleSim){
            publisher_move = n.advertise<geometry_msgs::Twist>(TOPIC_TURTLE_MOVE, buffer_size);  
        }else{
            publisher_move = n.advertise<geometry_msgs::Twist>(TOPIC_MOVE, buffer_size); 
        }
    }

    void moveMessage( geometry_msgs::Twist message ){
        publisher_move.publish(message);
    }
    
    void moveMessage(int x, int y){
        geometry_msgs::Twist msg;
        msg.linear.x = x ;
        msg.linear.y = y ;
        msg.linear.z = 0 ;

        msg.angular.x = 0 ;
        msg.angular.y = 0 ;
        msg.angular.z = 0 ;

        publisher_move.publish(msg);
    }
    void rotMessage(double r){
        geometry_msgs::Twist msg;
        msg.linear.x = 0 ;
        msg.linear.y = 0 ;
        msg.linear.z = 0 ;

        msg.angular.x = 0 ;
        msg.angular.y = 0 ;
        msg.angular.z = r ;
        publisher_move.publish(msg);
    }


};